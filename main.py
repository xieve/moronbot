#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Simple Bot to reply to Telegram messages.
This program is dedicated to the public domain under the CC0 license.
This Bot uses the Updater class to handle the bot.
First, a few handler functions are defined. Then, those functions are passed to
the Dispatcher and registered at their respective places.
Then, the bot is started and runs until we press Ctrl-C on the command line.
Usage:
Basic Echobot example, repeats messages.
Press Ctrl-C on the command line or send a signal to the process to stop the
bot.
"""

filters = []

from telegram.ext import Updater, CommandHandler, MessageHandler, Filters, BaseFilter
import telegram.bot as bot
import logging

# Enable logging
logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                    level=logging.INFO)

logger = logging.getLogger(__name__)


class adminFilter(BaseFilter):
    def filter(self, message):
        userStatus = bot.getChatMember(message.chat.id, message.user.id).status
        return userStatus == 'administrator' or userStatus == 'owner'


class spamFilter(BaseFilter):
    def filter(self, message):
        for filter in filters:
            if filter in message.text:
                return True

        return False


# Define a few command handlers. These usually take the two arguments bot and
# update. Error handlers also receive the raised TelegramError object in error.
def start(bot, update):
    """Send a message when the command /start is issued."""
    update.message.reply_text('Hi! I\'m a crappy bot created for the morons of @dailyEmChat. I\'m mostly circlejerk and inside jokes so you may as well just go now.')


def help(bot, update):
    """Send a message when the command /help is issued."""
    update.message.reply_text('If this is an actual plea for help in response to a hazardous material spill, an explosion, a fire on your person, radiation poisoning, a choking gas of unknown origin, eye trauma resulting from the use of an emergency eye wash station on floors three, four, or eleven, an animal malfunction, or any other injurious experimental equipment failure, please remain at your workstation. A Crisis Response Team has already been mobilized to deliberate on a response to your crisis.\n\nIf you need help accessing the system, please refer to your User Handbook.')


def text(bot, update):
    if update.message.text == '+':
        update.message.reply_text('Reputation of someone increased (am too dumb for this sorry)')
    elif 'pepe' in update.message.text.lower():
        update.message.reply_text('YOU A NAZI')


def newFilter(bot, update, args):
    filters.append(args)


def spamFilterCB(bot, update):
    update.message.reply_text('TRIGGERED')

def sticker(bot, update):
    if update.message.sticker.set_name == 'AbuApustajaBack':
        update.message.reply_text('YOU A NAZI')


def error(bot, update, error):
    """Log Errors caused by Updates."""
    logger.warning('Update "%s" caused error "%s"', update, error)


def main():
    """Start the bot."""
    # Create the EventHandler and pass it your bot's token.
    updater = Updater("682537682:AAFVsWlm12PsMk6LD-LzdCHdNw5lXynCm_0")

    # Get the dispatcher to register handlers
    dp = updater.dispatcher

    # on different commands - answer in Telegram
    dp.add_handler(CommandHandler("start", start))
    dp.add_handler(CommandHandler("help", help))

    # admin tools
    dp.add_handler(CommandHandler('newfilter', newFilter, adminFilter, pass_args=True, pass_user_data=True, pass_chat_data=True))

    # on noncommand i.e message - check for filters and triggers
    dp.add_handler(MessageHandler(Filters.text, text))
    dp.add_handler(MessageHandler(Filters.sticker, sticker))
    dp.add_handler(MessageHandler(Filters.text & spamFilter, spamFilterCB))

    # log all errors
    dp.add_error_handler(error)

    # Start the Bot and let it run indefinitely
    updater.start_polling()
    updater.idle()


if __name__ == '__main__':
    main()
